from . import exceptions, utils
from .ccache import Krb5CCache
from .wrappers.base import Krb5Wrapper
from .wrappers.context import Krb5ContextWrapper


class Krb5Context(Krb5Wrapper):
    def __init__(self, secure=False):
        self.secure = secure
        if self.secure:
            context = Krb5ContextWrapper.init_secure()
        else:
            context = Krb5ContextWrapper.init()
        super().__init__(obj=context)

    def __enter__(self):
        return self

    def __exit__(self, *args, **kwargs):
        self.wrapper.free()

    @property
    def object(self):
        return super().object.object

    @property
    def wrapper(self):
        return super().object

    def check_code(self, code):
        if code == 0 or code is None:
            return
        kwargs = {"error_code": code}
        try:
            kwargs["context"] = self.object
        except Krb5Wrapper.FreedObject:
            pass
        raise exceptions.get_exception(**kwargs)

    def to_principal(self, principal):
        return utils.to_principal(self, principal)

    def to_data(self, data):
        return utils.to_data(self, data)

    def resolve_ccache(self, ccache_name):
        return Krb5CCache.resolve(self, ccache_name)

    def new_unique_ccache(self, ccache_type="MEMORY", default_principal=None):
        return Krb5CCache.new_unique(self, ccache_type, default_principal)

    def get_or_create_ccache(self, ccache_name=None, default_principal=None):
        ccache = self.resolve_ccache(ccache_name)
        try:
            current_principal = ccache.default_principal
            if not default_principal or current_principal.compare(default_principal):
                return ccache
        except exceptions.Krb5FccNofile:
            pass
        if default_principal is None:
            raise RuntimeError(
                "Unable to initialize ccache without a default principal"
            )
        ccache.initialize(default_principal)
        return ccache
