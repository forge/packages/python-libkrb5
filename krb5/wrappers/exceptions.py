from ..exceptions import Krb5Error


class Krb5CRandsourceExternalProtocol(Krb5Error):
    name = "KRB5_C_RANDSOURCE_EXTERNAL_PROTOCOL"
    error_code = 4


class Krb5CRandsourceMax(Krb5Error):
    name = "KRB5_C_RANDSOURCE_MAX"
    error_code = 5


class Krb5CRandsourceOldapi(Krb5Error):
    name = "KRB5_C_RANDSOURCE_OLDAPI"
    error_code = 0


class Krb5CRandsourceOsrand(Krb5Error):
    name = "KRB5_C_RANDSOURCE_OSRAND"
    error_code = 1


class Krb5CRandsourceTiming(Krb5Error):
    name = "KRB5_C_RANDSOURCE_TIMING"
    error_code = 3


class Krb5CRandsourceTrustedparty(Krb5Error):
    name = "KRB5_C_RANDSOURCE_TRUSTEDPARTY"
    error_code = 2


class Krb5Deprecated(Krb5Error):
    name = "KRB5_DEPRECATED"
    error_code = 0


class Krb5Int32Max(Krb5Error):
    name = "KRB5_INT32_MAX"
    error_code = 2147483647


class Krb5Int16Max(Krb5Error):
    name = "KRB5_INT16_MAX"
    error_code = 65535


class Krb5NtUnknown(Krb5Error):
    name = "KRB5_NT_UNKNOWN"
    error_code = 0


class Krb5NtPrincipal(Krb5Error):
    name = "KRB5_NT_PRINCIPAL"
    error_code = 1


class Krb5NtSrvInst(Krb5Error):
    name = "KRB5_NT_SRV_INST"
    error_code = 2


class Krb5NtSrvHst(Krb5Error):
    name = "KRB5_NT_SRV_HST"
    error_code = 3


class Krb5NtSrvXhst(Krb5Error):
    name = "KRB5_NT_SRV_XHST"
    error_code = 4


class Krb5NtUid(Krb5Error):
    name = "KRB5_NT_UID"
    error_code = 5


class Krb5NtX500Principal(Krb5Error):
    name = "KRB5_NT_X500_PRINCIPAL"
    error_code = 6


class Krb5NtSmtpName(Krb5Error):
    name = "KRB5_NT_SMTP_NAME"
    error_code = 7


class Krb5NtEnterprisePrincipal(Krb5Error):
    name = "KRB5_NT_ENTERPRISE_PRINCIPAL"
    error_code = 10


class Krb5NtWellknown(Krb5Error):
    name = "KRB5_NT_WELLKNOWN"
    error_code = 11


class Krb5NtMsPrincipal(Krb5Error):
    name = "KRB5_NT_MS_PRINCIPAL"
    error_code = -128


class Krb5NtMsPrincipalAndId(Krb5Error):
    name = "KRB5_NT_MS_PRINCIPAL_AND_ID"
    error_code = -129


class Krb5NtEntPrincipalAndId(Krb5Error):
    name = "KRB5_NT_ENT_PRINCIPAL_AND_ID"
    error_code = -130


class KdcTktCommonMask(Krb5Error):
    name = "KDC_TKT_COMMON_MASK"
    error_code = 1417674752


class MsecDirbit(Krb5Error):
    name = "MSEC_DIRBIT"
    error_code = 32768


class MsecValMask(Krb5Error):
    name = "MSEC_VAL_MASK"
    error_code = 32767


class Krb5Pvno(Krb5Error):
    name = "KRB5_PVNO"
    error_code = 5


class Krb5LrqNone(Krb5Error):
    name = "KRB5_LRQ_NONE"
    error_code = 0


class Krb5LrqAllLastTgt(Krb5Error):
    name = "KRB5_LRQ_ALL_LAST_TGT"
    error_code = 1


class Krb5LrqOneLastTgt(Krb5Error):
    name = "KRB5_LRQ_ONE_LAST_TGT"
    error_code = -1


class Krb5LrqAllLastInitial(Krb5Error):
    name = "KRB5_LRQ_ALL_LAST_INITIAL"
    error_code = 2


class Krb5LrqOneLastInitial(Krb5Error):
    name = "KRB5_LRQ_ONE_LAST_INITIAL"
    error_code = -2


class Krb5LrqAllLastTgtIssued(Krb5Error):
    name = "KRB5_LRQ_ALL_LAST_TGT_ISSUED"
    error_code = 3


class Krb5LrqOneLastTgtIssued(Krb5Error):
    name = "KRB5_LRQ_ONE_LAST_TGT_ISSUED"
    error_code = -3


class Krb5LrqAllLastRenewal(Krb5Error):
    name = "KRB5_LRQ_ALL_LAST_RENEWAL"
    error_code = 4


class Krb5LrqOneLastRenewal(Krb5Error):
    name = "KRB5_LRQ_ONE_LAST_RENEWAL"
    error_code = -4


class Krb5LrqAllLastReq(Krb5Error):
    name = "KRB5_LRQ_ALL_LAST_REQ"
    error_code = 5


class Krb5LrqOneLastReq(Krb5Error):
    name = "KRB5_LRQ_ONE_LAST_REQ"
    error_code = -5


class Krb5LrqAllPwExptime(Krb5Error):
    name = "KRB5_LRQ_ALL_PW_EXPTIME"
    error_code = 6


class Krb5LrqOnePwExptime(Krb5Error):
    name = "KRB5_LRQ_ONE_PW_EXPTIME"
    error_code = -6


class Krb5LrqAllAcctExptime(Krb5Error):
    name = "KRB5_LRQ_ALL_ACCT_EXPTIME"
    error_code = 7


class Krb5LrqOneAcctExptime(Krb5Error):
    name = "KRB5_LRQ_ONE_ACCT_EXPTIME"
    error_code = -7


class Krb5PadataNone(Krb5Error):
    name = "KRB5_PADATA_NONE"
    error_code = 0


class Krb5PadataApReq(Krb5Error):
    name = "KRB5_PADATA_AP_REQ"
    error_code = 1


class Krb5PadataEncTimestamp(Krb5Error):
    name = "KRB5_PADATA_ENC_TIMESTAMP"
    error_code = 2


class Krb5PadataPwSalt(Krb5Error):
    name = "KRB5_PADATA_PW_SALT"
    error_code = 3


class Krb5PadataEncEnckey(Krb5Error):
    name = "KRB5_PADATA_ENC_ENCKEY"
    error_code = 4


class Krb5PadataEncUnixTime(Krb5Error):
    name = "KRB5_PADATA_ENC_UNIX_TIME"
    error_code = 5


class Krb5PadataEncSandiaSecurid(Krb5Error):
    name = "KRB5_PADATA_ENC_SANDIA_SECURID"
    error_code = 6


class Krb5PadataSesame(Krb5Error):
    name = "KRB5_PADATA_SESAME"
    error_code = 7


class Krb5PadataOsfDce(Krb5Error):
    name = "KRB5_PADATA_OSF_DCE"
    error_code = 8


class Krb5CybersafeSecureid(Krb5Error):
    name = "KRB5_CYBERSAFE_SECUREID"
    error_code = 9


class Krb5PadataAfs3Salt(Krb5Error):
    name = "KRB5_PADATA_AFS3_SALT"
    error_code = 10


class Krb5PadataSamChallenge(Krb5Error):
    name = "KRB5_PADATA_SAM_CHALLENGE"
    error_code = 12


class Krb5PadataSamResponse(Krb5Error):
    name = "KRB5_PADATA_SAM_RESPONSE"
    error_code = 13


class Krb5PadataPkAsReqOld(Krb5Error):
    name = "KRB5_PADATA_PK_AS_REQ_OLD"
    error_code = 14


class Krb5PadataPkAsRepOld(Krb5Error):
    name = "KRB5_PADATA_PK_AS_REP_OLD"
    error_code = 15


class Krb5PadataPkAsReq(Krb5Error):
    name = "KRB5_PADATA_PK_AS_REQ"
    error_code = 16


class Krb5PadataPkAsRep(Krb5Error):
    name = "KRB5_PADATA_PK_AS_REP"
    error_code = 17


class Krb5PadataUseSpecifiedKvno(Krb5Error):
    name = "KRB5_PADATA_USE_SPECIFIED_KVNO"
    error_code = 20


class Krb5PadataSvrReferralInfo(Krb5Error):
    name = "KRB5_PADATA_SVR_REFERRAL_INFO"
    error_code = 20


class Krb5PadataSamRedirect(Krb5Error):
    name = "KRB5_PADATA_SAM_REDIRECT"
    error_code = 21


class Krb5PadataReferral(Krb5Error):
    name = "KRB5_PADATA_REFERRAL"
    error_code = 25


class Krb5PadataSamChallenge2(Krb5Error):
    name = "KRB5_PADATA_SAM_CHALLENGE_2"
    error_code = 30


class Krb5PadataSamResponse2(Krb5Error):
    name = "KRB5_PADATA_SAM_RESPONSE_2"
    error_code = 31


class Krb5PadataPacRequest(Krb5Error):
    name = "KRB5_PADATA_PAC_REQUEST"
    error_code = 128


class Krb5PadataForUser(Krb5Error):
    name = "KRB5_PADATA_FOR_USER"
    error_code = 129


class Krb5PadataS4UX509User(Krb5Error):
    name = "KRB5_PADATA_S4U_X509_USER"
    error_code = 130


class Krb5PadataAsChecksum(Krb5Error):
    name = "KRB5_PADATA_AS_CHECKSUM"
    error_code = 132


class Krb5PadataFxCookie(Krb5Error):
    name = "KRB5_PADATA_FX_COOKIE"
    error_code = 133


class Krb5PadataFxFast(Krb5Error):
    name = "KRB5_PADATA_FX_FAST"
    error_code = 136


class Krb5PadataFxError(Krb5Error):
    name = "KRB5_PADATA_FX_ERROR"
    error_code = 137


class Krb5PadataEncryptedChallenge(Krb5Error):
    name = "KRB5_PADATA_ENCRYPTED_CHALLENGE"
    error_code = 138


class Krb5PadataOtpChallenge(Krb5Error):
    name = "KRB5_PADATA_OTP_CHALLENGE"
    error_code = 141


class Krb5PadataOtpRequest(Krb5Error):
    name = "KRB5_PADATA_OTP_REQUEST"
    error_code = 142


class Krb5PadataOtpPinChange(Krb5Error):
    name = "KRB5_PADATA_OTP_PIN_CHANGE"
    error_code = 144


class Krb5PadataPkinitKx(Krb5Error):
    name = "KRB5_PADATA_PKINIT_KX"
    error_code = 147


class Krb5EncpadataReqEncPaRep(Krb5Error):
    name = "KRB5_ENCPADATA_REQ_ENC_PA_REP"
    error_code = 149


class Krb5PadataAsFreshness(Krb5Error):
    name = "KRB5_PADATA_AS_FRESHNESS"
    error_code = 150


class Krb5PadataSpake(Krb5Error):
    name = "KRB5_PADATA_SPAKE"
    error_code = 151


class Krb5SamUseSadAsKey(Krb5Error):
    name = "KRB5_SAM_USE_SAD_AS_KEY"
    error_code = 2147483648


class Krb5SamSendEncryptedSad(Krb5Error):
    name = "KRB5_SAM_SEND_ENCRYPTED_SAD"
    error_code = 1073741824


class Krb5SamMustPkEncryptSad(Krb5Error):
    name = "KRB5_SAM_MUST_PK_ENCRYPT_SAD"
    error_code = 536870912


class Krb5DomainX500Compress(Krb5Error):
    name = "KRB5_DOMAIN_X500_COMPRESS"
    error_code = 1


class Krb5AltauthAttChallengeResponse(Krb5Error):
    name = "KRB5_ALTAUTH_ATT_CHALLENGE_RESPONSE"
    error_code = 64


class Krb5AuthdataIfRelevant(Krb5Error):
    name = "KRB5_AUTHDATA_IF_RELEVANT"
    error_code = 1


class Krb5AuthdataKdcIssued(Krb5Error):
    name = "KRB5_AUTHDATA_KDC_ISSUED"
    error_code = 4


class Krb5AuthdataAndOr(Krb5Error):
    name = "KRB5_AUTHDATA_AND_OR"
    error_code = 5


class Krb5AuthdataMandatoryForKdc(Krb5Error):
    name = "KRB5_AUTHDATA_MANDATORY_FOR_KDC"
    error_code = 8


class Krb5AuthdataInitialVerifiedCas(Krb5Error):
    name = "KRB5_AUTHDATA_INITIAL_VERIFIED_CAS"
    error_code = 9


class Krb5AuthdataOsfDce(Krb5Error):
    name = "KRB5_AUTHDATA_OSF_DCE"
    error_code = 64


class Krb5AuthdataSesame(Krb5Error):
    name = "KRB5_AUTHDATA_SESAME"
    error_code = 65


class Krb5AuthdataCammac(Krb5Error):
    name = "KRB5_AUTHDATA_CAMMAC"
    error_code = 96


class Krb5AuthdataWin2KPac(Krb5Error):
    name = "KRB5_AUTHDATA_WIN2K_PAC"
    error_code = 128


class Krb5AuthdataSignticket(Krb5Error):
    name = "KRB5_AUTHDATA_SIGNTICKET"
    error_code = 512


class Krb5AuthdataFxArmor(Krb5Error):
    name = "KRB5_AUTHDATA_FX_ARMOR"
    error_code = 71


class Krb5AuthdataAuthIndicator(Krb5Error):
    name = "KRB5_AUTHDATA_AUTH_INDICATOR"
    error_code = 97


class Krb5KpasswdSuccess(Krb5Error):
    name = "KRB5_KPASSWD_SUCCESS"
    error_code = 0


class Krb5KpasswdMalformed(Krb5Error):
    name = "KRB5_KPASSWD_MALFORMED"
    error_code = 1


class Krb5KpasswdHarderror(Krb5Error):
    name = "KRB5_KPASSWD_HARDERROR"
    error_code = 2


class Krb5KpasswdAutherror(Krb5Error):
    name = "KRB5_KPASSWD_AUTHERROR"
    error_code = 3


class Krb5KpasswdSofterror(Krb5Error):
    name = "KRB5_KPASSWD_SOFTERROR"
    error_code = 4


class Krb5KpasswdAccessdenied(Krb5Error):
    name = "KRB5_KPASSWD_ACCESSDENIED"
    error_code = 5


class Krb5KpasswdBadVersion(Krb5Error):
    name = "KRB5_KPASSWD_BAD_VERSION"
    error_code = 6


class Krb5KpasswdInitialFlagNeeded(Krb5Error):
    name = "KRB5_KPASSWD_INITIAL_FLAG_NEEDED"
    error_code = 7


class Krb5AuthContextDoTime(Krb5Error):
    name = "KRB5_AUTH_CONTEXT_DO_TIME"
    error_code = 1


class Krb5AuthContextRetTime(Krb5Error):
    name = "KRB5_AUTH_CONTEXT_RET_TIME"
    error_code = 2


class Krb5AuthContextDoSequence(Krb5Error):
    name = "KRB5_AUTH_CONTEXT_DO_SEQUENCE"
    error_code = 4


class Krb5AuthContextRetSequence(Krb5Error):
    name = "KRB5_AUTH_CONTEXT_RET_SEQUENCE"
    error_code = 8


class Krb5AuthContextPermitAll(Krb5Error):
    name = "KRB5_AUTH_CONTEXT_PERMIT_ALL"
    error_code = 16


class Krb5AuthContextUseSubkey(Krb5Error):
    name = "KRB5_AUTH_CONTEXT_USE_SUBKEY"
    error_code = 32


class Krb5AuthContextGenerateLocalAddr(Krb5Error):
    name = "KRB5_AUTH_CONTEXT_GENERATE_LOCAL_ADDR"
    error_code = 1


class Krb5AuthContextGenerateRemoteAddr(Krb5Error):
    name = "KRB5_AUTH_CONTEXT_GENERATE_REMOTE_ADDR"
    error_code = 2


class Krb5AuthContextGenerateLocalFullAddr(Krb5Error):
    name = "KRB5_AUTH_CONTEXT_GENERATE_LOCAL_FULL_ADDR"
    error_code = 4


class Krb5AuthContextGenerateRemoteFullAddr(Krb5Error):
    name = "KRB5_AUTH_CONTEXT_GENERATE_REMOTE_FULL_ADDR"
    error_code = 8


class Krb5TcMatchTimes(Krb5Error):
    name = "KRB5_TC_MATCH_TIMES"
    error_code = 1


class Krb5TcMatchIsSkey(Krb5Error):
    name = "KRB5_TC_MATCH_IS_SKEY"
    error_code = 2


class Krb5TcMatchFlags(Krb5Error):
    name = "KRB5_TC_MATCH_FLAGS"
    error_code = 4


class Krb5TcMatchTimesExact(Krb5Error):
    name = "KRB5_TC_MATCH_TIMES_EXACT"
    error_code = 8


class Krb5TcMatchFlagsExact(Krb5Error):
    name = "KRB5_TC_MATCH_FLAGS_EXACT"
    error_code = 16


class Krb5TcMatchAuthdata(Krb5Error):
    name = "KRB5_TC_MATCH_AUTHDATA"
    error_code = 32


class Krb5TcMatchSrvNameonly(Krb5Error):
    name = "KRB5_TC_MATCH_SRV_NAMEONLY"
    error_code = 64


class Krb5TcMatch2NdTkt(Krb5Error):
    name = "KRB5_TC_MATCH_2ND_TKT"
    error_code = 128


class Krb5TcOpenclose(Krb5Error):
    name = "KRB5_TC_OPENCLOSE"
    error_code = 1


class Krb5TcNoticket(Krb5Error):
    name = "KRB5_TC_NOTICKET"
    error_code = 2


class MaxKeytabNameLen(Krb5Error):
    name = "MAX_KEYTAB_NAME_LEN"
    error_code = 1100


class Krb5InitContextSecure(Krb5Error):
    name = "KRB5_INIT_CONTEXT_SECURE"
    error_code = 1


class Krb5InitContextKdc(Krb5Error):
    name = "KRB5_INIT_CONTEXT_KDC"
    error_code = 2


class Krb5GcUserUser(Krb5Error):
    name = "KRB5_GC_USER_USER"
    error_code = 1


class Krb5GcCached(Krb5Error):
    name = "KRB5_GC_CACHED"
    error_code = 2


class Krb5GcCanonicalize(Krb5Error):
    name = "KRB5_GC_CANONICALIZE"
    error_code = 4


class Krb5GcNoStore(Krb5Error):
    name = "KRB5_GC_NO_STORE"
    error_code = 8


class Krb5GcForwardable(Krb5Error):
    name = "KRB5_GC_FORWARDABLE"
    error_code = 16


class Krb5GcNoTransitCheck(Krb5Error):
    name = "KRB5_GC_NO_TRANSIT_CHECK"
    error_code = 32


class Krb5GcConstrainedDelegation(Krb5Error):
    name = "KRB5_GC_CONSTRAINED_DELEGATION"
    error_code = 64


class Krb5PrincipalParseNoRealm(Krb5Error):
    name = "KRB5_PRINCIPAL_PARSE_NO_REALM"
    error_code = 1


class Krb5PrincipalParseRequireRealm(Krb5Error):
    name = "KRB5_PRINCIPAL_PARSE_REQUIRE_REALM"
    error_code = 2


class Krb5PrincipalParseEnterprise(Krb5Error):
    name = "KRB5_PRINCIPAL_PARSE_ENTERPRISE"
    error_code = 4


class Krb5PrincipalParseIgnoreRealm(Krb5Error):
    name = "KRB5_PRINCIPAL_PARSE_IGNORE_REALM"
    error_code = 8


class Krb5PrincipalUnparseShort(Krb5Error):
    name = "KRB5_PRINCIPAL_UNPARSE_SHORT"
    error_code = 1


class Krb5PrincipalUnparseNoRealm(Krb5Error):
    name = "KRB5_PRINCIPAL_UNPARSE_NO_REALM"
    error_code = 2


class Krb5PrincipalUnparseDisplay(Krb5Error):
    name = "KRB5_PRINCIPAL_UNPARSE_DISPLAY"
    error_code = 4


class Krb5PrincipalCompareIgnoreRealm(Krb5Error):
    name = "KRB5_PRINCIPAL_COMPARE_IGNORE_REALM"
    error_code = 1


class Krb5PrincipalCompareEnterprise(Krb5Error):
    name = "KRB5_PRINCIPAL_COMPARE_ENTERPRISE"
    error_code = 2


class Krb5PrincipalCompareCasefold(Krb5Error):
    name = "KRB5_PRINCIPAL_COMPARE_CASEFOLD"
    error_code = 4


class Krb5PrincipalCompareUtf8(Krb5Error):
    name = "KRB5_PRINCIPAL_COMPARE_UTF8"
    error_code = 8


class Krb5TgsNameSize(Krb5Error):
    name = "KRB5_TGS_NAME_SIZE"
    error_code = 6


class Krb5RecvauthSkipVersion(Krb5Error):
    name = "KRB5_RECVAUTH_SKIP_VERSION"
    error_code = 1


class Krb5RecvauthBadauthvers(Krb5Error):
    name = "KRB5_RECVAUTH_BADAUTHVERS"
    error_code = 2


class Krb5ResponderOtpFormatDecimal(Krb5Error):
    name = "KRB5_RESPONDER_OTP_FORMAT_DECIMAL"
    error_code = 0


class Krb5ResponderOtpFormatHexadecimal(Krb5Error):
    name = "KRB5_RESPONDER_OTP_FORMAT_HEXADECIMAL"
    error_code = 1


class Krb5ResponderOtpFormatAlphanumeric(Krb5Error):
    name = "KRB5_RESPONDER_OTP_FORMAT_ALPHANUMERIC"
    error_code = 2


class Krb5ResponderOtpFlagsCollectToken(Krb5Error):
    name = "KRB5_RESPONDER_OTP_FLAGS_COLLECT_TOKEN"
    error_code = 1


class Krb5ResponderOtpFlagsCollectPin(Krb5Error):
    name = "KRB5_RESPONDER_OTP_FLAGS_COLLECT_PIN"
    error_code = 2


class Krb5ResponderOtpFlagsNextotp(Krb5Error):
    name = "KRB5_RESPONDER_OTP_FLAGS_NEXTOTP"
    error_code = 4


class Krb5ResponderOtpFlagsSeparatePin(Krb5Error):
    name = "KRB5_RESPONDER_OTP_FLAGS_SEPARATE_PIN"
    error_code = 8


class Krb5ResponderPkinitFlagsTokenUserPinCountLow(Krb5Error):
    name = "KRB5_RESPONDER_PKINIT_FLAGS_TOKEN_USER_PIN_COUNT_LOW"
    error_code = 1


class Krb5ResponderPkinitFlagsTokenUserPinFinalTry(Krb5Error):
    name = "KRB5_RESPONDER_PKINIT_FLAGS_TOKEN_USER_PIN_FINAL_TRY"
    error_code = 1


class Krb5ResponderPkinitFlagsTokenUserPinLocked(Krb5Error):
    name = "KRB5_RESPONDER_PKINIT_FLAGS_TOKEN_USER_PIN_LOCKED"
    error_code = 1


class Krb5FastRequired(Krb5Error):
    name = "KRB5_FAST_REQUIRED"
    error_code = 1


class Krb5InitCredsStepFlagContinue(Krb5Error):
    name = "KRB5_INIT_CREDS_STEP_FLAG_CONTINUE"
    error_code = 1


class Krb5TktCredsStepFlagContinue(Krb5Error):
    name = "KRB5_TKT_CREDS_STEP_FLAG_CONTINUE"
    error_code = 1


class Krb5PacLogonInfo(Krb5Error):
    name = "KRB5_PAC_LOGON_INFO"
    error_code = 1


class Krb5PacCredentialsInfo(Krb5Error):
    name = "KRB5_PAC_CREDENTIALS_INFO"
    error_code = 2


class Krb5PacServerChecksum(Krb5Error):
    name = "KRB5_PAC_SERVER_CHECKSUM"
    error_code = 6


class Krb5PacPrivsvrChecksum(Krb5Error):
    name = "KRB5_PAC_PRIVSVR_CHECKSUM"
    error_code = 7


class Krb5PacClientInfo(Krb5Error):
    name = "KRB5_PAC_CLIENT_INFO"
    error_code = 10


class Krb5PacDelegationInfo(Krb5Error):
    name = "KRB5_PAC_DELEGATION_INFO"
    error_code = 11


class Krb5PacUpnDnsInfo(Krb5Error):
    name = "KRB5_PAC_UPN_DNS_INFO"
    error_code = 12


class Krb5KdcErrNone(Krb5Error):
    name = "KRB5KDC_ERR_NONE"
    error_code = -1765328384


class Krb5KdcErrNameExp(Krb5Error):
    name = "KRB5KDC_ERR_NAME_EXP"
    error_code = -1765328383


class Krb5KdcErrServiceExp(Krb5Error):
    name = "KRB5KDC_ERR_SERVICE_EXP"
    error_code = -1765328382


class Krb5KdcErrBadPvno(Krb5Error):
    name = "KRB5KDC_ERR_BAD_PVNO"
    error_code = -1765328381


class Krb5KdcErrCOldMastKvno(Krb5Error):
    name = "KRB5KDC_ERR_C_OLD_MAST_KVNO"
    error_code = -1765328380


class Krb5KdcErrSOldMastKvno(Krb5Error):
    name = "KRB5KDC_ERR_S_OLD_MAST_KVNO"
    error_code = -1765328379


class Krb5KdcErrCPrincipalUnknown(Krb5Error):
    name = "KRB5KDC_ERR_C_PRINCIPAL_UNKNOWN"
    error_code = -1765328378


class Krb5KdcErrSPrincipalUnknown(Krb5Error):
    name = "KRB5KDC_ERR_S_PRINCIPAL_UNKNOWN"
    error_code = -1765328377


class Krb5KdcErrPrincipalNotUnique(Krb5Error):
    name = "KRB5KDC_ERR_PRINCIPAL_NOT_UNIQUE"
    error_code = -1765328376


class Krb5KdcErrNullKey(Krb5Error):
    name = "KRB5KDC_ERR_NULL_KEY"
    error_code = -1765328375


class Krb5KdcErrCannotPostdate(Krb5Error):
    name = "KRB5KDC_ERR_CANNOT_POSTDATE"
    error_code = -1765328374


class Krb5KdcErrNeverValid(Krb5Error):
    name = "KRB5KDC_ERR_NEVER_VALID"
    error_code = -1765328373


class Krb5KdcErrPolicy(Krb5Error):
    name = "KRB5KDC_ERR_POLICY"
    error_code = -1765328372


class Krb5KdcErrClientRevoked(Krb5Error):
    name = "KRB5KDC_ERR_CLIENT_REVOKED"
    error_code = -1765328366


class Krb5KdcErrServiceRevoked(Krb5Error):
    name = "KRB5KDC_ERR_SERVICE_REVOKED"
    error_code = -1765328365


class Krb5KdcErrTgtRevoked(Krb5Error):
    name = "KRB5KDC_ERR_TGT_REVOKED"
    error_code = -1765328364


class Krb5KdcErrClientNotyet(Krb5Error):
    name = "KRB5KDC_ERR_CLIENT_NOTYET"
    error_code = -1765328363


class Krb5KdcErrServiceNotyet(Krb5Error):
    name = "KRB5KDC_ERR_SERVICE_NOTYET"
    error_code = -1765328362


class Krb5KdcErrKeyExp(Krb5Error):
    name = "KRB5KDC_ERR_KEY_EXP"
    error_code = -1765328361


class Krb5KdcErrPreauthFailed(Krb5Error):
    name = "KRB5KDC_ERR_PREAUTH_FAILED"
    error_code = -1765328360


class Krb5KdcErrPreauthRequired(Krb5Error):
    name = "KRB5KDC_ERR_PREAUTH_REQUIRED"
    error_code = -1765328359


class Krb5KdcErrServerNomatch(Krb5Error):
    name = "KRB5KDC_ERR_SERVER_NOMATCH"
    error_code = -1765328358


class Krb5KdcErrMustUseUser2User(Krb5Error):
    name = "KRB5KDC_ERR_MUST_USE_USER2USER"
    error_code = -1765328357


class Krb5KdcErrPathNotAccepted(Krb5Error):
    name = "KRB5KDC_ERR_PATH_NOT_ACCEPTED"
    error_code = -1765328356


class Krb5KdcErrSvcUnavailable(Krb5Error):
    name = "KRB5KDC_ERR_SVC_UNAVAILABLE"
    error_code = -1765328355


class Krb5Placehold30(Krb5Error):
    name = "KRB5PLACEHOLD_30"
    error_code = -1765328354


class Krb5KrbApErrBadIntegrity(Krb5Error):
    name = "KRB5KRB_AP_ERR_BAD_INTEGRITY"
    error_code = -1765328353


class Krb5KrbApErrTktExpired(Krb5Error):
    name = "KRB5KRB_AP_ERR_TKT_EXPIRED"
    error_code = -1765328352


class Krb5KrbApErrTktNyv(Krb5Error):
    name = "KRB5KRB_AP_ERR_TKT_NYV"
    error_code = -1765328351


class Krb5KrbApErrRepeat(Krb5Error):
    name = "KRB5KRB_AP_ERR_REPEAT"
    error_code = -1765328350


class Krb5KrbApErrNotUs(Krb5Error):
    name = "KRB5KRB_AP_ERR_NOT_US"
    error_code = -1765328349


class Krb5KrbApErrBadmatch(Krb5Error):
    name = "KRB5KRB_AP_ERR_BADMATCH"
    error_code = -1765328348


class Krb5KrbApErrSkew(Krb5Error):
    name = "KRB5KRB_AP_ERR_SKEW"
    error_code = -1765328347


class Krb5KrbApErrBadaddr(Krb5Error):
    name = "KRB5KRB_AP_ERR_BADADDR"
    error_code = -1765328346


class Krb5KrbApErrBadversion(Krb5Error):
    name = "KRB5KRB_AP_ERR_BADVERSION"
    error_code = -1765328345


class Krb5KrbApErrModified(Krb5Error):
    name = "KRB5KRB_AP_ERR_MODIFIED"
    error_code = -1765328343


class Krb5KrbApErrBadorder(Krb5Error):
    name = "KRB5KRB_AP_ERR_BADORDER"
    error_code = -1765328342


class Krb5KrbApErrIllCrTkt(Krb5Error):
    name = "KRB5KRB_AP_ERR_ILL_CR_TKT"
    error_code = -1765328341


class Krb5KrbApErrBadkeyver(Krb5Error):
    name = "KRB5KRB_AP_ERR_BADKEYVER"
    error_code = -1765328340


class Krb5KrbApErrNokey(Krb5Error):
    name = "KRB5KRB_AP_ERR_NOKEY"
    error_code = -1765328339


class Krb5KrbApErrMutFail(Krb5Error):
    name = "KRB5KRB_AP_ERR_MUT_FAIL"
    error_code = -1765328338


class Krb5KrbApErrBaddirection(Krb5Error):
    name = "KRB5KRB_AP_ERR_BADDIRECTION"
    error_code = -1765328337


class Krb5KrbApErrMethod(Krb5Error):
    name = "KRB5KRB_AP_ERR_METHOD"
    error_code = -1765328336


class Krb5KrbApErrBadseq(Krb5Error):
    name = "KRB5KRB_AP_ERR_BADSEQ"
    error_code = -1765328335


class Krb5KrbApErrInappCksum(Krb5Error):
    name = "KRB5KRB_AP_ERR_INAPP_CKSUM"
    error_code = -1765328334


class Krb5KrbApPathNotAccepted(Krb5Error):
    name = "KRB5KRB_AP_PATH_NOT_ACCEPTED"
    error_code = -1765328333


class Krb5KrbErrResponseTooBig(Krb5Error):
    name = "KRB5KRB_ERR_RESPONSE_TOO_BIG"
    error_code = -1765328332


class Krb5Placehold53(Krb5Error):
    name = "KRB5PLACEHOLD_53"
    error_code = -1765328331


class Krb5Placehold54(Krb5Error):
    name = "KRB5PLACEHOLD_54"
    error_code = -1765328330


class Krb5Placehold55(Krb5Error):
    name = "KRB5PLACEHOLD_55"
    error_code = -1765328329


class Krb5Placehold56(Krb5Error):
    name = "KRB5PLACEHOLD_56"
    error_code = -1765328328


class Krb5Placehold57(Krb5Error):
    name = "KRB5PLACEHOLD_57"
    error_code = -1765328327


class Krb5Placehold58(Krb5Error):
    name = "KRB5PLACEHOLD_58"
    error_code = -1765328326


class Krb5Placehold59(Krb5Error):
    name = "KRB5PLACEHOLD_59"
    error_code = -1765328325


class Krb5KrbErrGeneric(Krb5Error):
    name = "KRB5KRB_ERR_GENERIC"
    error_code = -1765328324


class Krb5KrbErrFieldToolong(Krb5Error):
    name = "KRB5KRB_ERR_FIELD_TOOLONG"
    error_code = -1765328323


class Krb5KdcErrClientNotTrusted(Krb5Error):
    name = "KRB5KDC_ERR_CLIENT_NOT_TRUSTED"
    error_code = -1765328322


class Krb5KdcErrKdcNotTrusted(Krb5Error):
    name = "KRB5KDC_ERR_KDC_NOT_TRUSTED"
    error_code = -1765328321


class Krb5KdcErrInvalidSig(Krb5Error):
    name = "KRB5KDC_ERR_INVALID_SIG"
    error_code = -1765328320


class Krb5KdcErrDhKeyParametersNotAccepted(Krb5Error):
    name = "KRB5KDC_ERR_DH_KEY_PARAMETERS_NOT_ACCEPTED"
    error_code = -1765328319


class Krb5KdcErrCertificateMismatch(Krb5Error):
    name = "KRB5KDC_ERR_CERTIFICATE_MISMATCH"
    error_code = -1765328318


class Krb5KrbApErrNoTgt(Krb5Error):
    name = "KRB5KRB_AP_ERR_NO_TGT"
    error_code = -1765328317


class Krb5KdcErrWrongRealm(Krb5Error):
    name = "KRB5KDC_ERR_WRONG_REALM"
    error_code = -1765328316


class Krb5KrbApErrUserToUserRequired(Krb5Error):
    name = "KRB5KRB_AP_ERR_USER_TO_USER_REQUIRED"
    error_code = -1765328315


class Krb5KdcErrCantVerifyCertificate(Krb5Error):
    name = "KRB5KDC_ERR_CANT_VERIFY_CERTIFICATE"
    error_code = -1765328314


class Krb5KdcErrInvalidCertificate(Krb5Error):
    name = "KRB5KDC_ERR_INVALID_CERTIFICATE"
    error_code = -1765328313


class Krb5KdcErrRevokedCertificate(Krb5Error):
    name = "KRB5KDC_ERR_REVOKED_CERTIFICATE"
    error_code = -1765328312


class Krb5KdcErrRevocationStatusUnknown(Krb5Error):
    name = "KRB5KDC_ERR_REVOCATION_STATUS_UNKNOWN"
    error_code = -1765328311


class Krb5KdcErrRevocationStatusUnavailable(Krb5Error):
    name = "KRB5KDC_ERR_REVOCATION_STATUS_UNAVAILABLE"
    error_code = -1765328310


class Krb5KdcErrClientNameMismatch(Krb5Error):
    name = "KRB5KDC_ERR_CLIENT_NAME_MISMATCH"
    error_code = -1765328309


class Krb5KdcErrKdcNameMismatch(Krb5Error):
    name = "KRB5KDC_ERR_KDC_NAME_MISMATCH"
    error_code = -1765328308


class Krb5KdcErrInconsistentKeyPurpose(Krb5Error):
    name = "KRB5KDC_ERR_INCONSISTENT_KEY_PURPOSE"
    error_code = -1765328307


class Krb5KdcErrDigestInCertNotAccepted(Krb5Error):
    name = "KRB5KDC_ERR_DIGEST_IN_CERT_NOT_ACCEPTED"
    error_code = -1765328306


class Krb5KdcErrPaChecksumMustBeIncluded(Krb5Error):
    name = "KRB5KDC_ERR_PA_CHECKSUM_MUST_BE_INCLUDED"
    error_code = -1765328305


class Krb5KdcErrDigestInSignedDataNotAccepted(Krb5Error):
    name = "KRB5KDC_ERR_DIGEST_IN_SIGNED_DATA_NOT_ACCEPTED"
    error_code = -1765328304


class Krb5KdcErrPublicKeyEncryptionNotSupported(Krb5Error):
    name = "KRB5KDC_ERR_PUBLIC_KEY_ENCRYPTION_NOT_SUPPORTED"
    error_code = -1765328303


class Krb5Placehold82(Krb5Error):
    name = "KRB5PLACEHOLD_82"
    error_code = -1765328302


class Krb5Placehold83(Krb5Error):
    name = "KRB5PLACEHOLD_83"
    error_code = -1765328301


class Krb5Placehold84(Krb5Error):
    name = "KRB5PLACEHOLD_84"
    error_code = -1765328300


class Krb5KrbApErrIakerbKdcNotFound(Krb5Error):
    name = "KRB5KRB_AP_ERR_IAKERB_KDC_NOT_FOUND"
    error_code = -1765328299


class Krb5KrbApErrIakerbKdcNoResponse(Krb5Error):
    name = "KRB5KRB_AP_ERR_IAKERB_KDC_NO_RESPONSE"
    error_code = -1765328298


class Krb5Placehold87(Krb5Error):
    name = "KRB5PLACEHOLD_87"
    error_code = -1765328297


class Krb5Placehold88(Krb5Error):
    name = "KRB5PLACEHOLD_88"
    error_code = -1765328296


class Krb5Placehold89(Krb5Error):
    name = "KRB5PLACEHOLD_89"
    error_code = -1765328295


class Krb5KdcErrPreauthExpired(Krb5Error):
    name = "KRB5KDC_ERR_PREAUTH_EXPIRED"
    error_code = -1765328294


class Krb5KdcErrMorePreauthDataRequired(Krb5Error):
    name = "KRB5KDC_ERR_MORE_PREAUTH_DATA_REQUIRED"
    error_code = -1765328293


class Krb5Placehold92(Krb5Error):
    name = "KRB5PLACEHOLD_92"
    error_code = -1765328292


class Krb5Placehold94(Krb5Error):
    name = "KRB5PLACEHOLD_94"
    error_code = -1765328290


class Krb5Placehold95(Krb5Error):
    name = "KRB5PLACEHOLD_95"
    error_code = -1765328289


class Krb5Placehold96(Krb5Error):
    name = "KRB5PLACEHOLD_96"
    error_code = -1765328288


class Krb5Placehold97(Krb5Error):
    name = "KRB5PLACEHOLD_97"
    error_code = -1765328287


class Krb5Placehold98(Krb5Error):
    name = "KRB5PLACEHOLD_98"
    error_code = -1765328286


class Krb5Placehold99(Krb5Error):
    name = "KRB5PLACEHOLD_99"
    error_code = -1765328285


class Krb5KdcErrNoAcceptableKdf(Krb5Error):
    name = "KRB5KDC_ERR_NO_ACCEPTABLE_KDF"
    error_code = -1765328284


class Krb5Placehold101(Krb5Error):
    name = "KRB5PLACEHOLD_101"
    error_code = -1765328283


class Krb5Placehold102(Krb5Error):
    name = "KRB5PLACEHOLD_102"
    error_code = -1765328282


class Krb5Placehold103(Krb5Error):
    name = "KRB5PLACEHOLD_103"
    error_code = -1765328281


class Krb5Placehold104(Krb5Error):
    name = "KRB5PLACEHOLD_104"
    error_code = -1765328280


class Krb5Placehold105(Krb5Error):
    name = "KRB5PLACEHOLD_105"
    error_code = -1765328279


class Krb5Placehold106(Krb5Error):
    name = "KRB5PLACEHOLD_106"
    error_code = -1765328278


class Krb5Placehold107(Krb5Error):
    name = "KRB5PLACEHOLD_107"
    error_code = -1765328277


class Krb5Placehold108(Krb5Error):
    name = "KRB5PLACEHOLD_108"
    error_code = -1765328276


class Krb5Placehold109(Krb5Error):
    name = "KRB5PLACEHOLD_109"
    error_code = -1765328275


class Krb5Placehold110(Krb5Error):
    name = "KRB5PLACEHOLD_110"
    error_code = -1765328274


class Krb5Placehold111(Krb5Error):
    name = "KRB5PLACEHOLD_111"
    error_code = -1765328273


class Krb5Placehold112(Krb5Error):
    name = "KRB5PLACEHOLD_112"
    error_code = -1765328272


class Krb5Placehold113(Krb5Error):
    name = "KRB5PLACEHOLD_113"
    error_code = -1765328271


class Krb5Placehold114(Krb5Error):
    name = "KRB5PLACEHOLD_114"
    error_code = -1765328270


class Krb5Placehold115(Krb5Error):
    name = "KRB5PLACEHOLD_115"
    error_code = -1765328269


class Krb5Placehold116(Krb5Error):
    name = "KRB5PLACEHOLD_116"
    error_code = -1765328268


class Krb5Placehold117(Krb5Error):
    name = "KRB5PLACEHOLD_117"
    error_code = -1765328267


class Krb5Placehold118(Krb5Error):
    name = "KRB5PLACEHOLD_118"
    error_code = -1765328266


class Krb5Placehold119(Krb5Error):
    name = "KRB5PLACEHOLD_119"
    error_code = -1765328265


class Krb5Placehold120(Krb5Error):
    name = "KRB5PLACEHOLD_120"
    error_code = -1765328264


class Krb5Placehold121(Krb5Error):
    name = "KRB5PLACEHOLD_121"
    error_code = -1765328263


class Krb5Placehold122(Krb5Error):
    name = "KRB5PLACEHOLD_122"
    error_code = -1765328262


class Krb5Placehold123(Krb5Error):
    name = "KRB5PLACEHOLD_123"
    error_code = -1765328261


class Krb5Placehold124(Krb5Error):
    name = "KRB5PLACEHOLD_124"
    error_code = -1765328260


class Krb5Placehold125(Krb5Error):
    name = "KRB5PLACEHOLD_125"
    error_code = -1765328259


class Krb5Placehold126(Krb5Error):
    name = "KRB5PLACEHOLD_126"
    error_code = -1765328258


class Krb5Placehold127(Krb5Error):
    name = "KRB5PLACEHOLD_127"
    error_code = -1765328257


class Krb5ErrRcsid(Krb5Error):
    name = "KRB5_ERR_RCSID"
    error_code = -1765328256


class Krb5LibosBadlockflag(Krb5Error):
    name = "KRB5_LIBOS_BADLOCKFLAG"
    error_code = -1765328255


class Krb5LibosCantreadpwd(Krb5Error):
    name = "KRB5_LIBOS_CANTREADPWD"
    error_code = -1765328254


class Krb5LibosBadpwdmatch(Krb5Error):
    name = "KRB5_LIBOS_BADPWDMATCH"
    error_code = -1765328253


class Krb5LibosPwdintr(Krb5Error):
    name = "KRB5_LIBOS_PWDINTR"
    error_code = -1765328252


class Krb5ParseIllchar(Krb5Error):
    name = "KRB5_PARSE_ILLCHAR"
    error_code = -1765328251


class Krb5ParseMalformed(Krb5Error):
    name = "KRB5_PARSE_MALFORMED"
    error_code = -1765328250


class Krb5ConfigCantopen(Krb5Error):
    name = "KRB5_CONFIG_CANTOPEN"
    error_code = -1765328249


class Krb5ConfigBadformat(Krb5Error):
    name = "KRB5_CONFIG_BADFORMAT"
    error_code = -1765328248


class Krb5ConfigNotenufspace(Krb5Error):
    name = "KRB5_CONFIG_NOTENUFSPACE"
    error_code = -1765328247


class Krb5CcBadname(Krb5Error):
    name = "KRB5_CC_BADNAME"
    error_code = -1765328245


class Krb5CcNotfound(Krb5Error):
    name = "KRB5_CC_NOTFOUND"
    error_code = -1765328243


class Krb5CcEnd(Krb5Error):
    name = "KRB5_CC_END"
    error_code = -1765328242


class Krb5NoTktSupplied(Krb5Error):
    name = "KRB5_NO_TKT_SUPPLIED"
    error_code = -1765328241


class Krb5KrbApWrongPrinc(Krb5Error):
    name = "KRB5KRB_AP_WRONG_PRINC"
    error_code = -1765328240


class Krb5KrbApErrTktInvalid(Krb5Error):
    name = "KRB5KRB_AP_ERR_TKT_INVALID"
    error_code = -1765328239


class Krb5PrincNomatch(Krb5Error):
    name = "KRB5_PRINC_NOMATCH"
    error_code = -1765328238


class Krb5KdcrepModified(Krb5Error):
    name = "KRB5_KDCREP_MODIFIED"
    error_code = -1765328237


class Krb5KdcrepSkew(Krb5Error):
    name = "KRB5_KDCREP_SKEW"
    error_code = -1765328236


class Krb5InTktRealmMismatch(Krb5Error):
    name = "KRB5_IN_TKT_REALM_MISMATCH"
    error_code = -1765328235


class Krb5RealmUnknown(Krb5Error):
    name = "KRB5_REALM_UNKNOWN"
    error_code = -1765328230


class Krb5ServiceUnknown(Krb5Error):
    name = "KRB5_SERVICE_UNKNOWN"
    error_code = -1765328229


class Krb5KdcUnreach(Krb5Error):
    name = "KRB5_KDC_UNREACH"
    error_code = -1765328228


class Krb5NoLocalname(Krb5Error):
    name = "KRB5_NO_LOCALNAME"
    error_code = -1765328227


class Krb5MutualFailed(Krb5Error):
    name = "KRB5_MUTUAL_FAILED"
    error_code = -1765328226


class Krb5RcMalloc(Krb5Error):
    name = "KRB5_RC_MALLOC"
    error_code = -1765328224


class Krb5RcUnknown(Krb5Error):
    name = "KRB5_RC_UNKNOWN"
    error_code = -1765328222


class Krb5RcReplay(Krb5Error):
    name = "KRB5_RC_REPLAY"
    error_code = -1765328221


class Krb5RcIo(Krb5Error):
    name = "KRB5_RC_IO"
    error_code = -1765328220


class Krb5RcNoio(Krb5Error):
    name = "KRB5_RC_NOIO"
    error_code = -1765328219


class Krb5RcParse(Krb5Error):
    name = "KRB5_RC_PARSE"
    error_code = -1765328218


class Krb5RcIoEof(Krb5Error):
    name = "KRB5_RC_IO_EOF"
    error_code = -1765328217


class Krb5RcIoMalloc(Krb5Error):
    name = "KRB5_RC_IO_MALLOC"
    error_code = -1765328216


class Krb5RcIoPerm(Krb5Error):
    name = "KRB5_RC_IO_PERM"
    error_code = -1765328215


class Krb5RcIoIo(Krb5Error):
    name = "KRB5_RC_IO_IO"
    error_code = -1765328214


class Krb5RcIoUnknown(Krb5Error):
    name = "KRB5_RC_IO_UNKNOWN"
    error_code = -1765328213


class Krb5RcIoSpace(Krb5Error):
    name = "KRB5_RC_IO_SPACE"
    error_code = -1765328212


class Krb5TransCantopen(Krb5Error):
    name = "KRB5_TRANS_CANTOPEN"
    error_code = -1765328211


class Krb5TransBadformat(Krb5Error):
    name = "KRB5_TRANS_BADFORMAT"
    error_code = -1765328210


class Krb5LnameCantopen(Krb5Error):
    name = "KRB5_LNAME_CANTOPEN"
    error_code = -1765328209


class Krb5LnameNotrans(Krb5Error):
    name = "KRB5_LNAME_NOTRANS"
    error_code = -1765328208


class Krb5LnameBadformat(Krb5Error):
    name = "KRB5_LNAME_BADFORMAT"
    error_code = -1765328207


class Krb5CryptoInternal(Krb5Error):
    name = "KRB5_CRYPTO_INTERNAL"
    error_code = -1765328206


class Krb5KtBadname(Krb5Error):
    name = "KRB5_KT_BADNAME"
    error_code = -1765328205


class Krb5KtNotfound(Krb5Error):
    name = "KRB5_KT_NOTFOUND"
    error_code = -1765328203


class Krb5KtEnd(Krb5Error):
    name = "KRB5_KT_END"
    error_code = -1765328202


class Krb5KtNowrite(Krb5Error):
    name = "KRB5_KT_NOWRITE"
    error_code = -1765328201


class Krb5KtIoerr(Krb5Error):
    name = "KRB5_KT_IOERR"
    error_code = -1765328200


class Krb5NoTktInRlm(Krb5Error):
    name = "KRB5_NO_TKT_IN_RLM"
    error_code = -1765328199


class Krb5DesBadKeypar(Krb5Error):
    name = "KRB5DES_BAD_KEYPAR"
    error_code = -1765328198


class Krb5DesWeakKey(Krb5Error):
    name = "KRB5DES_WEAK_KEY"
    error_code = -1765328197


class Krb5BadKeysize(Krb5Error):
    name = "KRB5_BAD_KEYSIZE"
    error_code = -1765328195


class Krb5BadMsize(Krb5Error):
    name = "KRB5_BAD_MSIZE"
    error_code = -1765328194


class Krb5CcIo(Krb5Error):
    name = "KRB5_CC_IO"
    error_code = -1765328191


class Krb5FccPerm(Krb5Error):
    name = "KRB5_FCC_PERM"
    error_code = -1765328190


class Krb5FccNofile(Krb5Error):
    name = "KRB5_FCC_NOFILE"
    error_code = -1765328189


class Krb5FccInternal(Krb5Error):
    name = "KRB5_FCC_INTERNAL"
    error_code = -1765328188


class Krb5CcWrite(Krb5Error):
    name = "KRB5_CC_WRITE"
    error_code = -1765328187


class Krb5CcNomem(Krb5Error):
    name = "KRB5_CC_NOMEM"
    error_code = -1765328186


class Krb5CcFormat(Krb5Error):
    name = "KRB5_CC_FORMAT"
    error_code = -1765328185


class Krb5InvalidFlags(Krb5Error):
    name = "KRB5_INVALID_FLAGS"
    error_code = -1765328183


class Krb5No2NdTkt(Krb5Error):
    name = "KRB5_NO_2ND_TKT"
    error_code = -1765328182


class Krb5NocredsSupplied(Krb5Error):
    name = "KRB5_NOCREDS_SUPPLIED"
    error_code = -1765328181


class Krb5SendauthBadauthvers(Krb5Error):
    name = "KRB5_SENDAUTH_BADAUTHVERS"
    error_code = -1765328180


class Krb5SendauthBadapplvers(Krb5Error):
    name = "KRB5_SENDAUTH_BADAPPLVERS"
    error_code = -1765328179


class Krb5SendauthBadresponse(Krb5Error):
    name = "KRB5_SENDAUTH_BADRESPONSE"
    error_code = -1765328178


class Krb5SendauthRejected(Krb5Error):
    name = "KRB5_SENDAUTH_REJECTED"
    error_code = -1765328177


class Krb5PreauthNoKey(Krb5Error):
    name = "KRB5_PREAUTH_NO_KEY"
    error_code = -1765328175


class Krb5PreauthFailed(Krb5Error):
    name = "KRB5_PREAUTH_FAILED"
    error_code = -1765328174


class Krb5RcacheBadvno(Krb5Error):
    name = "KRB5_RCACHE_BADVNO"
    error_code = -1765328173


class Krb5CcacheBadvno(Krb5Error):
    name = "KRB5_CCACHE_BADVNO"
    error_code = -1765328172


class Krb5KeytabBadvno(Krb5Error):
    name = "KRB5_KEYTAB_BADVNO"
    error_code = -1765328171


class Krb5RcRequired(Krb5Error):
    name = "KRB5_RC_REQUIRED"
    error_code = -1765328169


class Krb5ErrBadHostname(Krb5Error):
    name = "KRB5_ERR_BAD_HOSTNAME"
    error_code = -1765328168


class Krb5ErrHostRealmUnknown(Krb5Error):
    name = "KRB5_ERR_HOST_REALM_UNKNOWN"
    error_code = -1765328167


class Krb5KrbApErrV4Reply(Krb5Error):
    name = "KRB5KRB_AP_ERR_V4_REPLY"
    error_code = -1765328165


class Krb5RealmCantResolve(Krb5Error):
    name = "KRB5_REALM_CANT_RESOLVE"
    error_code = -1765328164


class Krb5TktNotForwardable(Krb5Error):
    name = "KRB5_TKT_NOT_FORWARDABLE"
    error_code = -1765328163


class Krb5FwdBadPrincipal(Krb5Error):
    name = "KRB5_FWD_BAD_PRINCIPAL"
    error_code = -1765328162


class Krb5GetInTktLoop(Krb5Error):
    name = "KRB5_GET_IN_TKT_LOOP"
    error_code = -1765328161


class Krb5ConfigNodefrealm(Krb5Error):
    name = "KRB5_CONFIG_NODEFREALM"
    error_code = -1765328160


class Krb5SamUnsupported(Krb5Error):
    name = "KRB5_SAM_UNSUPPORTED"
    error_code = -1765328159


class Krb5SamNoChecksum(Krb5Error):
    name = "KRB5_SAM_NO_CHECKSUM"
    error_code = -1765328157


class Krb5SamBadChecksum(Krb5Error):
    name = "KRB5_SAM_BAD_CHECKSUM"
    error_code = -1765328156


class Krb5KtNameToolong(Krb5Error):
    name = "KRB5_KT_NAME_TOOLONG"
    error_code = -1765328155


class Krb5KtKvnonotfound(Krb5Error):
    name = "KRB5_KT_KVNONOTFOUND"
    error_code = -1765328154


class Krb5ApplExpired(Krb5Error):
    name = "KRB5_APPL_EXPIRED"
    error_code = -1765328153


class Krb5LibExpired(Krb5Error):
    name = "KRB5_LIB_EXPIRED"
    error_code = -1765328152


class Krb5ChpwPwdnull(Krb5Error):
    name = "KRB5_CHPW_PWDNULL"
    error_code = -1765328151


class Krb5ChpwFail(Krb5Error):
    name = "KRB5_CHPW_FAIL"
    error_code = -1765328150


class Krb5KtFormat(Krb5Error):
    name = "KRB5_KT_FORMAT"
    error_code = -1765328149


class Krb5ObsoleteFn(Krb5Error):
    name = "KRB5_OBSOLETE_FN"
    error_code = -1765328146


class Krb5EaiFail(Krb5Error):
    name = "KRB5_EAI_FAIL"
    error_code = -1765328145


class Krb5EaiNodata(Krb5Error):
    name = "KRB5_EAI_NODATA"
    error_code = -1765328144


class Krb5EaiNoname(Krb5Error):
    name = "KRB5_EAI_NONAME"
    error_code = -1765328143


class Krb5EaiService(Krb5Error):
    name = "KRB5_EAI_SERVICE"
    error_code = -1765328142


class Krb5ErrNumericRealm(Krb5Error):
    name = "KRB5_ERR_NUMERIC_REALM"
    error_code = -1765328141


class Krb5ErrBadS2KParams(Krb5Error):
    name = "KRB5_ERR_BAD_S2K_PARAMS"
    error_code = -1765328140


class Krb5ErrNoService(Krb5Error):
    name = "KRB5_ERR_NO_SERVICE"
    error_code = -1765328139


class Krb5CcReadonly(Krb5Error):
    name = "KRB5_CC_READONLY"
    error_code = -1765328138


class Krb5CcNosupp(Krb5Error):
    name = "KRB5_CC_NOSUPP"
    error_code = -1765328137


class Krb5DeltatBadformat(Krb5Error):
    name = "KRB5_DELTAT_BADFORMAT"
    error_code = -1765328136


class Krb5PluginNoHandle(Krb5Error):
    name = "KRB5_PLUGIN_NO_HANDLE"
    error_code = -1765328135


class Krb5PluginOpNotsupp(Krb5Error):
    name = "KRB5_PLUGIN_OP_NOTSUPP"
    error_code = -1765328134


class Krb5ErrInvalidUtf8(Krb5Error):
    name = "KRB5_ERR_INVALID_UTF8"
    error_code = -1765328133


class Krb5ErrFastRequired(Krb5Error):
    name = "KRB5_ERR_FAST_REQUIRED"
    error_code = -1765328132


class Krb5LocalAddrRequired(Krb5Error):
    name = "KRB5_LOCAL_ADDR_REQUIRED"
    error_code = -1765328131


class Krb5RemoteAddrRequired(Krb5Error):
    name = "KRB5_REMOTE_ADDR_REQUIRED"
    error_code = -1765328130


class Krb5TraceNosupp(Krb5Error):
    name = "KRB5_TRACE_NOSUPP"
    error_code = -1765328129


class Krb5PluginVerNotsupp(Krb5Error):
    name = "KRB5_PLUGIN_VER_NOTSUPP"
    error_code = -1750600192


class Krb5PluginBadModuleSpec(Krb5Error):
    name = "KRB5_PLUGIN_BAD_MODULE_SPEC"
    error_code = -1750600191


class Krb5PluginNameNotfound(Krb5Error):
    name = "KRB5_PLUGIN_NAME_NOTFOUND"
    error_code = -1750600190


class Krb5KdcErrDiscard(Krb5Error):
    name = "KRB5KDC_ERR_DISCARD"
    error_code = -1750600189


class Krb5DccCannotCreate(Krb5Error):
    name = "KRB5_DCC_CANNOT_CREATE"
    error_code = -1750600188


class Krb5KccInvalidAnchor(Krb5Error):
    name = "KRB5_KCC_INVALID_ANCHOR"
    error_code = -1750600187


class Krb5KccUnknownVersion(Krb5Error):
    name = "KRB5_KCC_UNKNOWN_VERSION"
    error_code = -1750600186


class Krb5KccInvalidUid(Krb5Error):
    name = "KRB5_KCC_INVALID_UID"
    error_code = -1750600185


class Krb5KcmMalformedReply(Krb5Error):
    name = "KRB5_KCM_MALFORMED_REPLY"
    error_code = -1750600184


class Krb5KcmRpcError(Krb5Error):
    name = "KRB5_KCM_RPC_ERROR"
    error_code = -1750600183


class Krb5KcmReplyTooBig(Krb5Error):
    name = "KRB5_KCM_REPLY_TOO_BIG"
    error_code = -1750600182


class Krb5KcmNoServer(Krb5Error):
    name = "KRB5_KCM_NO_SERVER"
    error_code = -1750600181


class Krb5KdbRcsid(Krb5Error):
    name = "KRB5_KDB_RCSID"
    error_code = -1780008448


class Krb5KdbInuse(Krb5Error):
    name = "KRB5_KDB_INUSE"
    error_code = -1780008447


class Krb5KdbUkSerror(Krb5Error):
    name = "KRB5_KDB_UK_SERROR"
    error_code = -1780008446


class Krb5KdbUkRerror(Krb5Error):
    name = "KRB5_KDB_UK_RERROR"
    error_code = -1780008445


class Krb5KdbUnauth(Krb5Error):
    name = "KRB5_KDB_UNAUTH"
    error_code = -1780008444


class Krb5KdbNoentry(Krb5Error):
    name = "KRB5_KDB_NOENTRY"
    error_code = -1780008443


class Krb5KdbIllWildcard(Krb5Error):
    name = "KRB5_KDB_ILL_WILDCARD"
    error_code = -1780008442


class Krb5KdbDbInuse(Krb5Error):
    name = "KRB5_KDB_DB_INUSE"
    error_code = -1780008441


class Krb5KdbDbChanged(Krb5Error):
    name = "KRB5_KDB_DB_CHANGED"
    error_code = -1780008440


class Krb5KdbTruncatedRecord(Krb5Error):
    name = "KRB5_KDB_TRUNCATED_RECORD"
    error_code = -1780008439


class Krb5KdbRecursivelock(Krb5Error):
    name = "KRB5_KDB_RECURSIVELOCK"
    error_code = -1780008438


class Krb5KdbNotlocked(Krb5Error):
    name = "KRB5_KDB_NOTLOCKED"
    error_code = -1780008437


class Krb5KdbBadlockmode(Krb5Error):
    name = "KRB5_KDB_BADLOCKMODE"
    error_code = -1780008436


class Krb5KdbDbnotinited(Krb5Error):
    name = "KRB5_KDB_DBNOTINITED"
    error_code = -1780008435


class Krb5KdbDbinited(Krb5Error):
    name = "KRB5_KDB_DBINITED"
    error_code = -1780008434


class Krb5KdbIlldirection(Krb5Error):
    name = "KRB5_KDB_ILLDIRECTION"
    error_code = -1780008433


class Krb5KdbNomasterkey(Krb5Error):
    name = "KRB5_KDB_NOMASTERKEY"
    error_code = -1780008432


class Krb5KdbBadmasterkey(Krb5Error):
    name = "KRB5_KDB_BADMASTERKEY"
    error_code = -1780008431


class Krb5KdbInvalidkeysize(Krb5Error):
    name = "KRB5_KDB_INVALIDKEYSIZE"
    error_code = -1780008430


class Krb5KdbCantreadStored(Krb5Error):
    name = "KRB5_KDB_CANTREAD_STORED"
    error_code = -1780008429


class Krb5KdbBadstoredMkey(Krb5Error):
    name = "KRB5_KDB_BADSTORED_MKEY"
    error_code = -1780008428


class Krb5KdbNoactmasterkey(Krb5Error):
    name = "KRB5_KDB_NOACTMASTERKEY"
    error_code = -1780008427


class Krb5KdbKvnonomatch(Krb5Error):
    name = "KRB5_KDB_KVNONOMATCH"
    error_code = -1780008426


class Krb5KdbStoredMkeyNotcurrent(Krb5Error):
    name = "KRB5_KDB_STORED_MKEY_NOTCURRENT"
    error_code = -1780008425


class Krb5KdbCantlockDb(Krb5Error):
    name = "KRB5_KDB_CANTLOCK_DB"
    error_code = -1780008424


class Krb5KdbDbCorrupt(Krb5Error):
    name = "KRB5_KDB_DB_CORRUPT"
    error_code = -1780008423


class Krb5KdbBadVersion(Krb5Error):
    name = "KRB5_KDB_BAD_VERSION"
    error_code = -1780008422


class Krb5KdbBadCreateflags(Krb5Error):
    name = "KRB5_KDB_BAD_CREATEFLAGS"
    error_code = -1780008419


class Krb5KdbNoPermittedKey(Krb5Error):
    name = "KRB5_KDB_NO_PERMITTED_KEY"
    error_code = -1780008418


class Krb5KdbNoMatchingKey(Krb5Error):
    name = "KRB5_KDB_NO_MATCHING_KEY"
    error_code = -1780008417


class Krb5KdbServerInternalErr(Krb5Error):
    name = "KRB5_KDB_SERVER_INTERNAL_ERR"
    error_code = -1780008413


class Krb5KdbAccessError(Krb5Error):
    name = "KRB5_KDB_ACCESS_ERROR"
    error_code = -1780008412


class Krb5KdbInternalError(Krb5Error):
    name = "KRB5_KDB_INTERNAL_ERROR"
    error_code = -1780008411


class Krb5KdbConstraintViolation(Krb5Error):
    name = "KRB5_KDB_CONSTRAINT_VIOLATION"
    error_code = -1780008410


class Krb5LogConv(Krb5Error):
    name = "KRB5_LOG_CONV"
    error_code = -1780008409


class Krb5LogUnstable(Krb5Error):
    name = "KRB5_LOG_UNSTABLE"
    error_code = -1780008408


class Krb5LogCorrupt(Krb5Error):
    name = "KRB5_LOG_CORRUPT"
    error_code = -1780008407


class Krb5LogError(Krb5Error):
    name = "KRB5_LOG_ERROR"
    error_code = -1780008406


class Krb5KdbPolicyRef(Krb5Error):
    name = "KRB5_KDB_POLICY_REF"
    error_code = -1780008404


class Krb5KdbStringsToolong(Krb5Error):
    name = "KRB5_KDB_STRINGS_TOOLONG"
    error_code = -1780008403


class Kv5MNone(Krb5Error):
    name = "KV5M_NONE"
    error_code = -1760647424


class Kv5MPrincipal(Krb5Error):
    name = "KV5M_PRINCIPAL"
    error_code = -1760647423


class Kv5MData(Krb5Error):
    name = "KV5M_DATA"
    error_code = -1760647422


class Kv5MKeyblock(Krb5Error):
    name = "KV5M_KEYBLOCK"
    error_code = -1760647421


class Kv5MChecksum(Krb5Error):
    name = "KV5M_CHECKSUM"
    error_code = -1760647420


class Kv5MEncryptBlock(Krb5Error):
    name = "KV5M_ENCRYPT_BLOCK"
    error_code = -1760647419


class Kv5MEncData(Krb5Error):
    name = "KV5M_ENC_DATA"
    error_code = -1760647418


class Kv5MCryptosystemEntry(Krb5Error):
    name = "KV5M_CRYPTOSYSTEM_ENTRY"
    error_code = -1760647417


class Kv5MCsTableEntry(Krb5Error):
    name = "KV5M_CS_TABLE_ENTRY"
    error_code = -1760647416


class Kv5MChecksumEntry(Krb5Error):
    name = "KV5M_CHECKSUM_ENTRY"
    error_code = -1760647415


class Kv5MAuthdata(Krb5Error):
    name = "KV5M_AUTHDATA"
    error_code = -1760647414


class Kv5MTransited(Krb5Error):
    name = "KV5M_TRANSITED"
    error_code = -1760647413


class Kv5MEncTktPart(Krb5Error):
    name = "KV5M_ENC_TKT_PART"
    error_code = -1760647412


class Kv5MTicket(Krb5Error):
    name = "KV5M_TICKET"
    error_code = -1760647411


class Kv5MAuthenticator(Krb5Error):
    name = "KV5M_AUTHENTICATOR"
    error_code = -1760647410


class Kv5MTktAuthent(Krb5Error):
    name = "KV5M_TKT_AUTHENT"
    error_code = -1760647409


class Kv5MCreds(Krb5Error):
    name = "KV5M_CREDS"
    error_code = -1760647408


class Kv5MLastReqEntry(Krb5Error):
    name = "KV5M_LAST_REQ_ENTRY"
    error_code = -1760647407


class Kv5MPaData(Krb5Error):
    name = "KV5M_PA_DATA"
    error_code = -1760647406


class Kv5MKdcReq(Krb5Error):
    name = "KV5M_KDC_REQ"
    error_code = -1760647405


class Kv5MEncKdcRepPart(Krb5Error):
    name = "KV5M_ENC_KDC_REP_PART"
    error_code = -1760647404


class Kv5MKdcRep(Krb5Error):
    name = "KV5M_KDC_REP"
    error_code = -1760647403


class Kv5MError(Krb5Error):
    name = "KV5M_ERROR"
    error_code = -1760647402


class Kv5MApReq(Krb5Error):
    name = "KV5M_AP_REQ"
    error_code = -1760647401


class Kv5MApRep(Krb5Error):
    name = "KV5M_AP_REP"
    error_code = -1760647400


class Kv5MApRepEncPart(Krb5Error):
    name = "KV5M_AP_REP_ENC_PART"
    error_code = -1760647399


class Kv5MResponse(Krb5Error):
    name = "KV5M_RESPONSE"
    error_code = -1760647398


class Kv5MSafe(Krb5Error):
    name = "KV5M_SAFE"
    error_code = -1760647397


class Kv5MPriv(Krb5Error):
    name = "KV5M_PRIV"
    error_code = -1760647396


class Kv5MPrivEncPart(Krb5Error):
    name = "KV5M_PRIV_ENC_PART"
    error_code = -1760647395


class Kv5MCred(Krb5Error):
    name = "KV5M_CRED"
    error_code = -1760647394


class Kv5MCredInfo(Krb5Error):
    name = "KV5M_CRED_INFO"
    error_code = -1760647393


class Kv5MCredEncPart(Krb5Error):
    name = "KV5M_CRED_ENC_PART"
    error_code = -1760647392


class Kv5MPwdData(Krb5Error):
    name = "KV5M_PWD_DATA"
    error_code = -1760647391


class Kv5MAddress(Krb5Error):
    name = "KV5M_ADDRESS"
    error_code = -1760647390


class Kv5MKeytabEntry(Krb5Error):
    name = "KV5M_KEYTAB_ENTRY"
    error_code = -1760647389


class Kv5MContext(Krb5Error):
    name = "KV5M_CONTEXT"
    error_code = -1760647388


class Kv5MOsContext(Krb5Error):
    name = "KV5M_OS_CONTEXT"
    error_code = -1760647387


class Kv5MAltMethod(Krb5Error):
    name = "KV5M_ALT_METHOD"
    error_code = -1760647386


class Kv5MDbContext(Krb5Error):
    name = "KV5M_DB_CONTEXT"
    error_code = -1760647384


class Kv5MAuthContext(Krb5Error):
    name = "KV5M_AUTH_CONTEXT"
    error_code = -1760647383


class Kv5MKeytab(Krb5Error):
    name = "KV5M_KEYTAB"
    error_code = -1760647382


class Kv5MRcache(Krb5Error):
    name = "KV5M_RCACHE"
    error_code = -1760647381


class Kv5MCcache(Krb5Error):
    name = "KV5M_CCACHE"
    error_code = -1760647380


class Kv5MPreauthOps(Krb5Error):
    name = "KV5M_PREAUTH_OPS"
    error_code = -1760647379


class Kv5MSamChallenge(Krb5Error):
    name = "KV5M_SAM_CHALLENGE"
    error_code = -1760647378


class Kv5MSamChallenge2(Krb5Error):
    name = "KV5M_SAM_CHALLENGE_2"
    error_code = -1760647377


class Kv5MSamKey(Krb5Error):
    name = "KV5M_SAM_KEY"
    error_code = -1760647376


class Kv5MEncSamResponseEnc(Krb5Error):
    name = "KV5M_ENC_SAM_RESPONSE_ENC"
    error_code = -1760647375


class Kv5MEncSamResponseEnc2(Krb5Error):
    name = "KV5M_ENC_SAM_RESPONSE_ENC_2"
    error_code = -1760647374


class Kv5MSamResponse(Krb5Error):
    name = "KV5M_SAM_RESPONSE"
    error_code = -1760647373


class Kv5MSamResponse2(Krb5Error):
    name = "KV5M_SAM_RESPONSE_2"
    error_code = -1760647372


class Kv5MPredictedSamResponse(Krb5Error):
    name = "KV5M_PREDICTED_SAM_RESPONSE"
    error_code = -1760647371


class Kv5MPasswdPhraseElement(Krb5Error):
    name = "KV5M_PASSWD_PHRASE_ELEMENT"
    error_code = -1760647370


class Kv5MGssOid(Krb5Error):
    name = "KV5M_GSS_OID"
    error_code = -1760647369


class Kv5MGssQueue(Krb5Error):
    name = "KV5M_GSS_QUEUE"
    error_code = -1760647368


class Kv5MFastArmoredReq(Krb5Error):
    name = "KV5M_FAST_ARMORED_REQ"
    error_code = -1760647367


class Kv5MFastReq(Krb5Error):
    name = "KV5M_FAST_REQ"
    error_code = -1760647366


class Kv5MFastResponse(Krb5Error):
    name = "KV5M_FAST_RESPONSE"
    error_code = -1760647365


class Kv5MAuthdataContext(Krb5Error):
    name = "KV5M_AUTHDATA_CONTEXT"
    error_code = -1760647364


class Krb524Badkey(Krb5Error):
    name = "KRB524_BADKEY"
    error_code = -1750206208


class Krb524Badaddr(Krb5Error):
    name = "KRB524_BADADDR"
    error_code = -1750206207


class Krb524Badprinc(Krb5Error):
    name = "KRB524_BADPRINC"
    error_code = -1750206206


class Krb524Badrealm(Krb5Error):
    name = "KRB524_BADREALM"
    error_code = -1750206205


class Krb524V4Err(Krb5Error):
    name = "KRB524_V4ERR"
    error_code = -1750206204


class Krb524Encfull(Krb5Error):
    name = "KRB524_ENCFULL"
    error_code = -1750206203


class Krb524Decempty(Krb5Error):
    name = "KRB524_DECEMPTY"
    error_code = -1750206202


class Krb524Notresp(Krb5Error):
    name = "KRB524_NOTRESP"
    error_code = -1750206201


class Krb524Krb4Disabled(Krb5Error):
    name = "KRB524_KRB4_DISABLED"
    error_code = -1750206200


class Asn1BadTimeformat(Krb5Error):
    name = "ASN1_BAD_TIMEFORMAT"
    error_code = 1859794432


class Asn1MissingField(Krb5Error):
    name = "ASN1_MISSING_FIELD"
    error_code = 1859794433


class Asn1MisplacedField(Krb5Error):
    name = "ASN1_MISPLACED_FIELD"
    error_code = 1859794434


class Asn1Overflow(Krb5Error):
    name = "ASN1_OVERFLOW"
    error_code = 1859794436


class Asn1Overrun(Krb5Error):
    name = "ASN1_OVERRUN"
    error_code = 1859794437


class Asn1BadId(Krb5Error):
    name = "ASN1_BAD_ID"
    error_code = 1859794438


class Asn1BadLength(Krb5Error):
    name = "ASN1_BAD_LENGTH"
    error_code = 1859794439


class Asn1BadFormat(Krb5Error):
    name = "ASN1_BAD_FORMAT"
    error_code = 1859794440


class Asn1ParseError(Krb5Error):
    name = "ASN1_PARSE_ERROR"
    error_code = 1859794441


class Asn1BadGmtime(Krb5Error):
    name = "ASN1_BAD_GMTIME"
    error_code = 1859794442


class Asn1MismatchIndef(Krb5Error):
    name = "ASN1_MISMATCH_INDEF"
    error_code = 1859794443


class Asn1MissingEoc(Krb5Error):
    name = "ASN1_MISSING_EOC"
    error_code = 1859794444


class Asn1Omitted(Krb5Error):
    name = "ASN1_OMITTED"
    error_code = 1859794445
