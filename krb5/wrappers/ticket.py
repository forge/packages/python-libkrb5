from .. import lib
from .base import Krb5WrapperWithContext
from .principal import Krb5StaticPrincipalWrapper


class Krb5TicketWrapper(Krb5WrapperWithContext):
    free_func = lib.krb5_free_ticket

    @property
    def server(self):
        return Krb5StaticPrincipalWrapper(
            context=self.context,
            parent=self,
            obj=self.object.contents.server,
        )
