from .. import lib, utils
from .base import Krb5WrapperWithContext
from .principal import Krb5StaticPrincipalWrapper, Krb5PrincipalWrapper
from .data import Krb5StaticDataTicketWrapper

import ctypes


class Krb5GetInitCredsOpt(Krb5WrapperWithContext):
    free_func = lib.krb5_get_init_creds_opt_free

    def __init__(self, context, *args, obj=None, **kwargs):
        if obj is None:
            obj = ctypes.POINTER(lib.krb5_get_init_creds_opt)()
            context.check_code(
                lib.krb5_get_init_creds_opt_alloc(
                    context.object,
                    ctypes.byref(obj),
                )
            )
        super().__init__(context, *args, obj=obj, **kwargs)

    @property
    def forwardable(self):
        return bool(self.object.contents.forwardable)

    @forwardable.setter
    def forwardable(self, value):
        lib.krb5_get_init_creds_opt_set_forwardable(
            self.object,
            1 if value else 0,
        )
        return bool(value)

    @property
    def in_ccache(self):
        from .ccache import Krb5CCacheWrapper
        ccache_wrapper = self.get_data("in_ccache")
        obj = Krb5CCacheWrapper(
            context=self.context,
            parent=ccache_wrapper,
            obj=self.object.contents.in_ccache,
        )
        obj.free_func = None
        return obj

    @in_ccache.setter
    def in_ccache(self, value):
        self.context.check_code(
            lib.krb5_get_init_creds_opt_set_in_ccache(
                self.context.object,
                self.object,
                value.object,
            )
        )
        value.add_ref(self)
        old_value = self.add_data(value, "in_ccache")
        if old_value:
            old_value.del_ref(self)
        return value


class Krb5CredsWrapper(Krb5WrapperWithContext):
    free_func = lib.krb5_free_creds

    @property
    def client(self):
        return Krb5StaticPrincipalWrapper(
            context=self.context,
            parent=self,
            obj=self.object.contents.client,
        )

    @client.setter
    def client(self, value):
        value = utils.to_principal(self.context, value)
        client = self.object.contents.client
        self.object.contents.client = None
        Krb5PrincipalWrapper(context=self.context, obj=client).free()
        if value is None:
            return None
        data = Krb5StaticPrincipalWrapper(
            context=self.context,
            parent=self,
            obj=self.object.contents.client,
        )
        return value.copy(data)

    @property
    def server(self):
        return Krb5StaticPrincipalWrapper(
            context=self.context,
            parent=self,
            obj=self.object.contents.server,
        )

    @server.setter
    def server(self, value):
        value = utils.to_principal(self.context, value)
        server = self.object.contents.server
        self.object.contents.server = None
        Krb5PrincipalWrapper(context=self.context, obj=server).free()
        if value is None:
            return None
        data = Krb5StaticPrincipalWrapper(
            context=self.context,
            parent=self,
            obj=self.object.contents.server,
        )
        return value.copy(data)

    @property
    def ticket(self):
        return Krb5StaticDataTicketWrapper(
            context=self.context,
            parent=self,
            obj=self.object.contents.ticket,
        )

    @ticket.setter
    def ticket(self, value):
        value = value.copy()
        value.free_func = None
        self.ticket.replace_contents(value.object.contents)
        self.add_data(value, "ticket")
        return self.ticket

    @property
    def second_ticket(self):
        return Krb5StaticDataTicketWrapper(
            context=self.context,
            parent=self,
            obj=self.object.contents.second_ticket,
        )

    @second_ticket.setter
    def second_ticket(self, value):
        value = value.copy()
        value.free_func = None
        self.second_ticket.replace_contents(value.object.contents)
        self.add_data(value, "second_ticket")
        return self.second_ticket


class Krb5StaticCredsWrapper(Krb5CredsWrapper):
    free_func = lib.krb5_free_cred_contents

    @classmethod
    def init_password(
        cls,
        *,
        context,
        client,
        password=None,
        prompter=None,
        data=None,
        start_time=0,
        in_tkt_service=None,
        k5_gic_options=None,
    ):
        if prompter is None:
            prompter = ctypes.cast(
                lib.krb5_prompter_posix,
                lib.krb5_prompter_fct,
            )

        cred = cls(context=context)
        context.check_code(
            lib.krb5_get_init_creds_password(
                context.object,
                cred.object,
                client.object,
                password,
                prompter,
                data,
                start_time,
                in_tkt_service.object if in_tkt_service is not None else None,
                k5_gic_options.object if k5_gic_options else None,
            )
        )
        return cred

    def __init__(self, *args, obj=None, **kwargs):
        if obj is None:
            obj = lib.krb5_creds()
            ctypes.memset(ctypes.byref(obj), 0, ctypes.sizeof(obj))
        super().__init__(*args, obj=ctypes.pointer(obj), **kwargs)
