from .. import lib, utils
from .base import Krb5WrapperWithContext
from .ticket import Krb5TicketWrapper

import ctypes


class Krb5DataWrapper(Krb5WrapperWithContext):
    free_func = lib.krb5_free_data

    def copy(self, data=None):
        if data is None:
            data = Krb5StaticDataWrapper(context=self.context)
        else:
            data._free()  # pylint: disable=protected-access
        self.context.check_code(
            lib.krb5_copy_data(
                self.context.object,
                self.object,
                ctypes.byref(data.object),
            )
        )
        return data


class Krb5StaticDataWrapper(Krb5DataWrapper):
    free_func = lib.krb5_free_data_contents

    def __init__(self, *args, obj=None, **kwargs):
        if obj is None:
            obj = lib.krb5_data()
            ctypes.memset(ctypes.byref(obj), 0, ctypes.sizeof(obj))
        super().__init__(*args, obj=ctypes.pointer(obj), **kwargs)


class Krb5StaticDataStringWrapper(Krb5StaticDataWrapper):
    free_func = None

    @classmethod
    def from_string(cls, context, string):
        string = bytes(string, "utf-8")
        data = ctypes.create_string_buffer(string)
        obj = cls(context=context)
        obj.add_data(data)
        obj.object.contents.magic = lib.KV5M_DATA
        obj.object.contents.data = data
        obj.object.contents.length = len(string)
        return obj

    def __str__(self):
        return utils.to_string(self)


class Krb5StaticDataTicketWrapper(Krb5StaticDataWrapper):
    free_func = None

    def decode(self):
        obj = ctypes.POINTER(lib.krb5_ticket)()
        self.context.check_code(lib.krb5_decode_ticket(self.object, ctypes.byref(obj)))
        return Krb5TicketWrapper(context=self.context, obj=obj)
