from .. import lib, exceptions
from .base import Krb5Wrapper

import ctypes


class Krb5ContextWrapper(Krb5Wrapper):
    free_func = lib.krb5_free_context

    @classmethod
    def init(cls):
        context = lib.krb5_context()
        exceptions.check_code_without_context(lib.krb5_init_context(ctypes.byref(context)))
        return cls(obj=context)

    @classmethod
    def init_secure(cls):
        context = lib.krb5_context()
        exceptions.check_code_without_context(
            lib.krb5_init_secure_context(ctypes.byref(context))
        )
        return cls(obj=context)
