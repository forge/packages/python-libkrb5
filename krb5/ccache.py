from . import exceptions, lib
from .wrappers.ccache import Krb5CCacheWrapper


class Krb5CCache:
    @classmethod
    def resolve(cls, context, name=None):
        if name is None:
            return cls(wrapper=Krb5CCacheWrapper.default(context))
        return cls(wrapper=Krb5CCacheWrapper.resolve(context, name))

    @classmethod
    def new_unique(cls, context, ccache_type="MEMORY", default_principal=None):
        ccache = cls(wrapper=Krb5CCacheWrapper.new_unique(context, ccache_type))
        if default_principal is not None:
            default_principal = context.to_principal(default_principal)
            ccache.initialize(default_principal=default_principal)
        return ccache

    def __init__(self, *, wrapper):
        self._wrapper = wrapper

    def __enter__(self):
        return self

    def __exit__(self, *args, **kwargs):
        self.wrapper.free()

    def __iter__(self):
        return iter(self.wrapper)

    def __getattr__(self, name):
        return getattr(self.wrapper, name)

    def __contains__(self, cred):
        try:
            self.get_credentials(
                in_creds=cred,
                options=lib.KRB5_GC_CACHED,
            )
        except exceptions.Krb5CcNotfound:
            return False
        return True

    @property
    def wrapper(self):
        return self._wrapper

    def remove_config(self, key, principal=None):
        self.wrapper.set_config(key, principal, None)

    def replace_config(self, key, principal=None, value=None):
        self.remove_config(key, principal)
        self.set_config(key, principal, value)
